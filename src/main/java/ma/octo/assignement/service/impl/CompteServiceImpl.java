package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CompteServiceImpl implements ICompteService {


    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private CompteRepository compteRepository;

    @Override
    public Compte getByNrCompte(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }

    @Override
    public List<Compte> getAll() {
        return compteRepository.findAll();
    }
}

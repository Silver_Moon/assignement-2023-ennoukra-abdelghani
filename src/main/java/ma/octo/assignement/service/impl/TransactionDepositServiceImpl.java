package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionDepositRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ITransactionDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static ma.octo.assignement.helpers.Constants.*;

@Slf4j
@Service
@Transactional
public class TransactionDepositServiceImpl implements ITransactionDepositService {


    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private TransactionDepositRepository transactionDepositRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private AuditDepositRepository auditDepositRepository;

    @Autowired
    private IAuditService auditService;
    @Override
    public void deposit(DepositDto depositDto) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException {
        Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRib());

        if(compteBeneficiaire == null){
            log.error("Le compte n'est existe pas.");
            throw new CompteNonExistantException("Le compte n'est exist pas.");
        }

        transactionValidate(depositDto.getMontant(), depositDto.getMotif());

//        add the amount to the beneficiare account
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));
//        save the change to database
        compteRepository.save(compteBeneficiaire);

//        create new Deposit Transaction
        TransactionDeposit transactionDeposit = new TransactionDeposit();
        transactionDeposit.setDateExecution(new Date());
        transactionDeposit.setCompteBeneficiaire(compteBeneficiaire);
        transactionDeposit.setRib(depositDto.getRib());
        transactionDeposit.setNomEmetteur(depositDto.getNomEmetteur());
        transactionDeposit.setMontant(depositDto.getMontant());
        transactionDeposit.setMotif(depositDto.getMotif());
//        save the trasaction to database
        transactionDepositRepository.save(transactionDeposit);


        String message = "Deposite de " + depositDto.getRib() + " d'un montant de  " + depositDto.getMontant();
        auditService.auditDeposit(message);


    }

    @Override
    public List<TransactionDeposit> getAllDeposit() {
        return transactionDepositRepository.findAll();
    }


    private void transactionValidate(BigDecimal montant, String motif) throws TransactionException {
//        check if the amount null or equal 0
        if(montant == null || montant.compareTo(BigDecimal.ZERO) == 0 ){
            log.error("Montant invalid ( vid )");
            throw new TransactionException("Montant invalid ( vid )");
        }
//        check if the amount less than the MIN_TRANSACTION AMOUNT
        else if(montant.compareTo(MIN_TRANSACTION_AMOUNT) < 0){
            log.error("Montant minimum de transfert non atteint");
            throw new TransactionException("Montant minimum de transfert non atteint");
        }
//        check if the amount is great than the MAX_TRANSACTION_AMOUNT
        else if(montant.compareTo(MAX_TRANSACTION_ANOUNT) > 0){
            log.error("Montant maximal du transfert dépassé");
            throw new TransactionException("Montant maximal du transfert dépassé");
        }

//        check if the motif is vid or blank
        if(motif == null || motif.isBlank()){
            log.error("Motif vid");
            throw new TransactionException("Motif vid");
        }
    }
}

package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.AuditTransferRepository;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransationTransferRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ITransactionTransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static ma.octo.assignement.helpers.Constants.MAX_TRANSACTION_ANOUNT;
import static ma.octo.assignement.helpers.Constants.MIN_TRANSACTION_AMOUNT;

@Slf4j
@Service
@Transactional
public class TransactionTransferServiceImpl implements ITransactionTransferService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private TransationTransferRepository transationTransferRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private AuditTransferRepository auditTransferRepository;

    @Autowired
    private IAuditService auditService;
    @Override
    public void transfer(TransferDto transferDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
//    first of all we need to get compte emetteur and beneficiaire
        Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());
        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());

//        check  if the comptes existe
        if(compteBeneficiaire == null || compteEmetteur == null){
            log.error("le compte n'existe pas");
            throw new CompteNonExistantException("le compte n'existe pas");
        }

//        check if the transaction valid
        transactionValidate(transferDto.getMontant(), transferDto.getMotif());
//
        if(compteEmetteur.getSolde().compareTo(transferDto.getMontant()) < 0){
            log.error("Sold insuffisent");
            throw new SoldeDisponibleInsuffisantException("Sold insuffisent");
        }
//          if all is good we subtract ( sold - montant )
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
//        save the change to database
        compteRepository.save(compteEmetteur);
//          create new transaction
        TransactionTransfer transactionTransfer = new TransactionTransfer();
        transactionTransfer.setDateExecution(new Date());
        transactionTransfer.setCompteEmetteur(compteEmetteur);
        transactionTransfer.setMotif(transferDto.getMotif());
        transactionTransfer.setMontant(transferDto.getMontant());
        transactionTransfer.setCompteBeneficiaire(compteBeneficiaire);
//          save the transaction to database
        transationTransferRepository.save(transactionTransfer);

        String message = "Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " +
        transferDto.getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant().toString();
        auditService.auditTransfer(message);
    }


    @Override
    public List<TransactionTransfer> getAllTransfers() {
        return transationTransferRepository.findAll();
    }


    private void transactionValidate(BigDecimal montant, String motif) throws TransactionException {
//        check if the amount null or equal 0
        if(montant == null || montant.compareTo(BigDecimal.ZERO) == 0 ){
            log.error("Montant invalid ( vid )");
            throw new TransactionException("Montant invalid ( vid )");
        }
//        check if the amount less than the MIN_TRANSACTION AMOUNT
        else if(montant.compareTo(MIN_TRANSACTION_AMOUNT) < 0){
            log.error("Montant minimum de transfert non atteint");
            throw new TransactionException("Montant minimum de transfert non atteint");
        }
//        check if the amount is great than the MAX_TRANSACTION_AMOUNT
        else if(montant.compareTo(MAX_TRANSACTION_ANOUNT) > 0){
            log.error("Montant maximal du transfert dépassé");
            throw new TransactionException("Montant maximal du transfert dépassé");
        }

//        check if the motif is vid or blank
        if(motif == null || motif.isBlank()){
            log.error("Motif vid");
            throw new TransactionException("Motif vid");
        }
    }
}

package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class UtililsateurServiceImpl implements IUtilisateurService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Override
    public List<Utilisateur> getAll() {
        return utilisateurRepository.findAll();
    }
}

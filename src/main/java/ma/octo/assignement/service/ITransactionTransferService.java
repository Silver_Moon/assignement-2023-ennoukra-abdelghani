package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface ITransactionTransferService {

//    implement the transfer method in the TransactionTransferServiceImpl
    void transfer(TransferDto transferDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException;

//    get all Transfer Transaction
    List<TransactionTransfer> getAllTransfers();
}

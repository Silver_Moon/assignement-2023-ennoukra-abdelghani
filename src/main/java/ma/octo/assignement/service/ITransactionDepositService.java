package ma.octo.assignement.service;

import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import javax.transaction.TransactionalException;
import java.util.List;

public interface ITransactionDepositService {
    void deposit(DepositDto depositDto) throws CompteNonExistantException, SoldeDisponibleInsuffisantException, TransactionException;

    List<TransactionDeposit> getAllDeposit();
}

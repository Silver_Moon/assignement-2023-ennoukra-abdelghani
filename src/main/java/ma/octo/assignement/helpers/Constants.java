package ma.octo.assignement.helpers;

import java.math.BigDecimal;

public class Constants {

    public static final BigDecimal MIN_TRANSACTION_AMOUNT = new BigDecimal(10);

    public static final BigDecimal MAX_TRANSACTION_AMOUNT_FOR_DAY = new BigDecimal(100000);

    public static final BigDecimal MAX_TRANSACTION_ANOUNT = new BigDecimal(1000);
}

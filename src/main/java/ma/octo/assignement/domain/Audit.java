package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;


@Setter
@Getter
@Entity
@Table(name = "AUDIT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Audit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 100)
    private String message;

    @Enumerated(EnumType.STRING)
    private EventType eventType;
}

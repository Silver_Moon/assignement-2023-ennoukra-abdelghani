package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
@Getter
@Setter
@Entity
@Table(name = "TRAN")
public class TransactionTransfer extends Transaction {

  @ManyToOne
  private Compte compteEmetteur;

}

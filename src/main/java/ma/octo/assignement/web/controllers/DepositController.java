package ma.octo.assignement.web.controllers;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.service.ITransactionDepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/transactionDeposit")
public class DepositController {
    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    @Autowired
    private ITransactionDepositService transactionDepositService;


    @GetMapping("/listDeposit")
    public List<DepositDto> getListDeposit(){
        LOGGER.info("Lister des deposits");
        return transactionDepositService.getAllDeposit()
                .stream()
                .map(DepositMapper::map)
                .collect(Collectors.toList());
    }

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void executerDeposit(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        transactionDepositService.deposit(depositDto);
    }

}

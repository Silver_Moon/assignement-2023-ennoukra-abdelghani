package ma.octo.assignement.web.controllers;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.service.impl.CompteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController()
@RequestMapping("/compte")
@Slf4j
public class CompteController {

    @Autowired
    private CompteServiceImpl compteService;

    @GetMapping("listComptes")
    List<CompteDto> getComptes(){
        return compteService.getAll()
                .stream()
                .map(CompteMapper::map)
                .collect(toList());
    }

}

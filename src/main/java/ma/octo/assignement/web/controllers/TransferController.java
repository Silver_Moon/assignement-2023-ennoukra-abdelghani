package ma.octo.assignement.web.controllers;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.ITransactionTransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController()
@RequestMapping("/transactionTransfer")
class TransferController {

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);


   @Autowired
    private ITransactionTransferService transactionTransferService;

    @GetMapping("listDesTransferts")
    List<TransferDto> loadAll() {
        LOGGER.info("Lister des utilisateurs");
        return transactionTransferService.getAllTransfers().
                stream()
                .map(TransferMapper::map)
                .collect(toList());
    }




    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
            transactionTransferService.transfer(transferDto);
    }
}

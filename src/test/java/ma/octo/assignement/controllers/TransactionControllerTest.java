package ma.octo.assignement.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private TransferDto transferDto;

    private DepositDto depositDto;


    @BeforeEach
    public void beforeEachTest(){
        transferDto = new TransferDto();
        transferDto.setMontant(new BigDecimal(100));
        transferDto.setMotif("motif");
        transferDto.setNrCompteBeneficiaire("010000A000001000"  );
        transferDto.setNrCompteEmetteur("010000B025001000");
        transferDto.setDate(new Date());

        depositDto = new DepositDto();
        depositDto.setMontant(new BigDecimal(1000));
        depositDto.setNomEmetteur("ennoukra");
        depositDto.setRib("RIB1");
        depositDto.setMotif("motif 2");
    }

    @AfterEach
    public void afterEachTest(){
        transferDto = null;
    }

    @Test
    public void getAllTransactionTransfer() throws Exception {
        mockMvc.perform(get("/transactionTransfer/listDesTransferts").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllDepositTransactions() throws Exception{
        mockMvc.perform(get("/transactionDeposit/listDeposit").accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void transferShouldBeSucceed() throws Exception{
        mockMvc.perform(
                        post("/transactionTransfer/executerTransfers")
                                .content(asJsonString(transferDto))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isCreated());
    }


    @Test
    public void depositSouldBeSucceed() throws Exception {
        mockMvc.perform(
                        post("/transactionDeposit/executerDeposit")
                                .content(asJsonString(depositDto))
                                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.context.support.UiApplicationContextUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UtilisateurRepositoryTest {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    private Utilisateur utilisateur;

    @BeforeEach
    public void beforeEachTest(){

        utilisateur = new Utilisateur();
        utilisateur.setUsername("SilverMoon");
        utilisateur.setFirstname("ennoukra");
        utilisateur.setLastname("abdelghani");
        utilisateur.setGender("Male");

        utilisateurRepository.deleteAll();

        utilisateurRepository.save(utilisateur);
        System.out.println("Utilisateur created with Id : " + utilisateur.getId());
    }

    @AfterEach
    public void afterEachTest(){
        utilisateur = null;
    }

    @Test
    public void findOn(){
        Optional<Utilisateur> result = utilisateurRepository.findById(utilisateur.getId());

        assertTrue(result.isPresent());
        assertEquals(utilisateur.getId(), result.get().getId());
    }


    @Test
    public void save(){
        Utilisateur utilisateur3 = new Utilisateur();
        utilisateur3.setUsername("utilisateur3");
        utilisateur3.setFirstname("utilisateur3");
        utilisateur3.setLastname("utilisateur3");
        utilisateur3.setGender("Female");

        Utilisateur utilisateurSaved = utilisateurRepository.save(utilisateur3);

        assertEquals("utilisateur3", utilisateurSaved.getUsername());
    }

    @Test
    public void deleteById(){
        utilisateurRepository.deleteById(utilisateur.getId());

        assertThat(utilisateurRepository.count()).isZero();
    }
}

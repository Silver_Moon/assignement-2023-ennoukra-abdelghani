package ma.octo.assignement.repository;

import ma.octo.assignement.domain.TransactionTransfer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransactionTransferRepositoryTest {

  @Autowired
  private TransationTransferRepository transferRepository;


  private TransactionTransfer transactionTransfer;


  @BeforeEach
  public void beforEachTest(){
    transactionTransfer = new TransactionTransfer();
    transactionTransfer.setMontant(BigDecimal.valueOf(2000));
    transferRepository.deleteAll();
    transferRepository.save(transactionTransfer);
    System.out.println("Transfer Transaction Id :" + transactionTransfer.getId());
  }

  @AfterEach
  public void afterEachTest(){
    transactionTransfer = null;
  }


  @Test
  public void findOne() {
    Optional<TransactionTransfer> result = transferRepository.findById(transactionTransfer.getId());

    assertTrue(result.isPresent());
    assertEquals(transactionTransfer.getId(), result.get().getId());
  }

  @Test
  public void save() {
    TransactionTransfer transactionTransfer2 = new TransactionTransfer();
    transactionTransfer2.setMontant(BigDecimal.valueOf(3000));

    TransactionTransfer savedTransactionTransfer = transferRepository.save(transactionTransfer2);

    assertEquals(3000, savedTransactionTransfer.getMontant().intValue());
  }

  @Test
  public void deleteById(){
    transferRepository.deleteById(transactionTransfer.getId());

    assertThat(transferRepository.count()).isZero();
  }
}
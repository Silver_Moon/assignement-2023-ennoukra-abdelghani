package ma.octo.assignement.repository;


import ma.octo.assignement.domain.TransactionDeposit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransactionDepositRepositoryTest {

    @Autowired
    private TransactionDepositRepository transactionDepositRepository;

    private TransactionDeposit transactionDeposit;

    @BeforeEach
    public void beforeEachTest(){
        transactionDeposit = new TransactionDeposit();
        transactionDeposit.setMontant(BigDecimal.valueOf(2500));
        transactionDepositRepository.deleteAll();
        transactionDepositRepository.save(transactionDeposit);
        System.out.println("Deposit Transaction Id :" + transactionDeposit.getId());

    }

    @AfterEach
    public void afterEachTest(){
        transactionDeposit = null;
    }

    @Test
    public void findOne(){
        Optional<TransactionDeposit> result = transactionDepositRepository.findById(transactionDeposit.getId());

        assertTrue(result.isPresent());
        assertEquals(transactionDeposit.getId(), result.get().getId());
    }
    @Test
    public void findAll(){
        TransactionDeposit transactionDeposit2 = new TransactionDeposit();
        transactionDeposit2.setMontant(BigDecimal.valueOf(3700));

        transactionDepositRepository.save(transactionDeposit2);

        List<TransactionDeposit> result = transactionDepositRepository.findAll();
        assertNotNull(result);
        assertEquals(2, result.size());

    }

    @Test
    public void save(){
        TransactionDeposit transactionDeposit2 = new TransactionDeposit();
        transactionDeposit2.setMontant(BigDecimal.valueOf(5500));

        TransactionDeposit transactionDepositSaved = transactionDepositRepository.save(transactionDeposit2);

        assertEquals(5500, transactionDepositSaved.getMontant().intValue());
    }
    @Test
    public void deleteById(){
        transactionDepositRepository.deleteById(transactionDeposit.getId());

        assertThat(transactionDepositRepository.count()).isZero();
    }

}

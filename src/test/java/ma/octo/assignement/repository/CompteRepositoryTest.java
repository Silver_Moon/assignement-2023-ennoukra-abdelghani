package ma.octo.assignement.repository;


import ma.octo.assignement.domain.Compte;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class CompteRepositoryTest {

    @Autowired
    private CompteRepository compteRepository;

    private Compte compte;

    @BeforeEach
    public void beforeEachTest(){
        compteRepository.deleteAll();
        compte = new Compte();
        compte.setNrCompte("compte1");
        compteRepository.save(compte);
        System.out.println("compte created with id : " + compte.getId());
    }

    @AfterEach
    public void afterEachTest(){
        compte = null;
    }

    @Test
    public void findOn(){
        Optional<Compte> result = compteRepository.findById(compte.getId());

        assertTrue(result.isPresent());
        assertEquals(compte.getId(), result.get().getId());
    }

    @Test
    public void findAll(){
        Compte compte2  = new Compte();
        compte2.setNrCompte("compte2");
        compteRepository.save(compte2);

        List<Compte> result = compteRepository.findAll();

        assertEquals(2, result.size());
    }

    @Test
    public void save(){
        Compte compte3 = new Compte();
        compte.setNrCompte("compte3");

        Compte compteSaved = compteRepository.save(compte3);

        assertEquals("compte3", compteSaved.getNrCompte());
    }

}

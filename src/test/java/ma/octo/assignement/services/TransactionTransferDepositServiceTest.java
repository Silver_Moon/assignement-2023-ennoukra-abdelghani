package ma.octo.assignement.services;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.TransactionDeposit;
import ma.octo.assignement.domain.TransactionTransfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.*;
import ma.octo.assignement.service.impl.TransactionDepositServiceImpl;
import ma.octo.assignement.service.impl.TransactionTransferServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TransactionTransferDepositServiceTest {

    @InjectMocks
    private TransactionTransferServiceImpl transactionTransferService;

    @Mock
    private TransationTransferRepository transferRepository;

    @Mock
    private CompteRepository compteRepository;

    @Mock
    private AuditTransferRepository auditTransferRepository;


    private Compte compteEmetteur;

    private Compte compteBeneficiare;

    private TransferDto transferDto;

    @BeforeEach
    public void beforeEach(){
        compteEmetteur = new Compte();
        compteEmetteur.setNrCompte("compteEmetter");
        compteEmetteur.setSolde(new BigDecimal(2000));

        compteBeneficiare = new Compte();
        compteBeneficiare.setNrCompte("compteBenefiiare");
        compteBeneficiare.setSolde(new BigDecimal(2000));

        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(compteEmetteur.getNrCompte());
        transferDto.setNrCompteBeneficiaire(compteBeneficiare.getNrCompte());
        transferDto.setMontant(new BigDecimal(100));
        transferDto.setMotif("motif");
    }

    @AfterEach
    public void afterEachTest(){
        compteEmetteur = null;
        compteBeneficiare = null;
        transferDto = null;
    }

    @Test
    void shouldGetAllTransfer(){
        List<TransactionTransfer> transfers = new ArrayList<>();
        transfers.add(new TransactionTransfer());
        transfers.add(new TransactionTransfer());

        given(transferRepository.findAll()).willReturn(transfers);

        assertEquals(transfers, transactionTransferService.getAllTransfers());

        verify(transferRepository).findAll();
    }

    @Test
    void transferShouldBeSucceed(){
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiare.getNrCompte())).willReturn(compteBeneficiare);

        assertDoesNotThrow(() -> transactionTransferService.transfer(transferDto));

        assertEquals(compteEmetteur.getSolde(), new BigDecimal(900));
        assertEquals(compteBeneficiare.getSolde(), new BigDecimal(1100));
        verify(compteRepository).save(compteEmetteur);
        verify(compteRepository).save(compteBeneficiare);
        verify(transferRepository).save(any());
        verify(auditTransferRepository).save(any());
    }


//    transfer shoud be fail because the amount is blanck
    @Test
    public void transferShouldBeFail(){
        transferDto.setMontant(null);

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiare.getNrCompte())).willReturn(compteBeneficiare);

        TransactionException transactionException = assertThrows(TransactionException.class,
                                                    () -> transactionTransferService.transfer(transferDto));
        assertTrue(transactionException.getMessage().contains("Montant invalid ( vid )"));
    }

//    transfer should be fail because the amount > Max amount
    @Test
    public void transferShouldBeFailMaxMontant(){
        compteEmetteur.setSolde(new BigDecimal(100000));
        transferDto.setMontant(new BigDecimal(999999));

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiare.getNrCompte())).willReturn(compteBeneficiare);

        TransactionException ex = assertThrows(TransactionException.class, () -> transactionTransferService.transfer(transferDto));
        assertTrue(ex.getMessage().contains("Montant maximal du transfert dépassé"));
    }

//    transfer should be fail because solde < amount
    @Test
    public void transferShouldBeFailSoldeInsuffisent(){
        compteEmetteur.setSolde(new BigDecimal(100));
        transferDto.setMontant(new BigDecimal(5000));

        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(compteEmetteur);
        given(compteRepository.findByNrCompte(compteBeneficiare.getNrCompte())).willReturn(compteBeneficiare);

        assertThrows(SoldeDisponibleInsuffisantException.class, () -> transactionTransferService.transfer(transferDto));
    }

    @Test
    public void transferShouldBeFailAccountDoesNotExist(){
        given(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).willReturn(null);
        given(compteRepository.findByNrCompte(compteBeneficiare.getNrCompte())).willReturn(compteBeneficiare);

        assertThrows(CompteNonExistantException.class, () -> transactionTransferService.transfer(transferDto));
    }




}

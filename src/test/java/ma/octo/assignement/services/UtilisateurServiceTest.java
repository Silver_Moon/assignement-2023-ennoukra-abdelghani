package ma.octo.assignement.services;


import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.impl.UtililsateurServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UtilisateurServiceTest {

    @InjectMocks
    private UtililsateurServiceImpl utililsateurService;

    @Mock
    private UtilisateurRepository utilisateurRepository;


    @Test
    void shouldGetAllUsers(){
        List<Utilisateur> users = new ArrayList<>();

        users.add(new Utilisateur());
        users.add(new Utilisateur());

        given(utilisateurRepository.findAll()).willReturn(users);

        assertEquals(users, utililsateurService.getAll());

        verify(utilisateurRepository).findAll();

    }
}
